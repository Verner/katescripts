var katescript = {
    "author": "Jonathan L. Verner <jonathan.verner@matfyz.cz>",
    "license": "LGPLv2+",
    "revision": 1,
    "kate-version": "5.1",
    "functions": ["from_hex"],
    "actions": [
        {   "function": "from_hex",
            "name": "Convert selection from HEX",
            "icon": "",
            "category": "Editing",
            "interactive": "false"
        }
    ]
}; // kate-script-header, must be at the start of the file without comments

require ("range.js");
require ("cursor.js");

function help(cmd) {
    if (cmd == "from_hex") {
        return i18n("Convert selected text from HEX into bytes");
    }
}

function from_hex() {
    const selection = view.selectedText();
    const selectionRange = view.selection();
    const exp = /[0-9a-fA-F]/;
    let bytes=[];
    let last='';
    for(let pos = 0; pos < selection.length; pos++) {
        if (selection[pos] == ' ') {
            if (last != '') {
                last = '0x'+last;
                bytes.push(Number(last));
            }
            last='';
        } else if (selection[pos] == '0' && selection[pos+1] == 'x') {
            if (last != '') {
                last = '0x'+last;
                bytes.push(Number(last));
            }
            last='';
            pos++;
        } else if (exp.test(selection[pos])) {
            last=last+selection[pos];
        } else {
            console.log("Error, unexpected character", selection[pos], "at", pos);
        }
        if (last.length==2) {
            last= '0x' + last;
            bytes.push(Number(last));
            last = '';
        }
    }
    let ret='';
    for(let b of bytes) {
        ret = ret + String.fromCharCode(b);
    }
    for(let i=0; i < bytes.length; i++) {
        ret = ret + String.fromCharCode(bytes[i]);
    }
    document.insertText(selectionRange.start, ret+" //");
}
