var katescript = {
    "author": "Jonathan L. Verner <jonathan.verner@matfyz.cz>",
    "license": "LGPLv2+",
    "revision": 1,
    "kate-version": "5.1",
    "functions": ["resolve_conflict", "next_conflict", "prev_conflict"],
    "actions": [
        {   "function": "resolve_conflict",
            "name": "Resolve current merge conflict",
            "icon": "",
            "category": "Editing",
            "interactive": "false"
        },
        {   "function": "next_conflict",
            "name": "Goto next unresolved merge conflict",
            "icon": "",
            "category": "Editing",
            "interactive": "false"
        },
        {   "function": "prev_conflict",
            "name": "Goto prev unresolved merge conflict",
            "icon": "",
            "category": "Editing",
            "interactive": "false"
        }
    ]
}; // kate-script-header, must be at the start of the file without comments

require ("range.js");
require ("cursor.js");

/**
 * Finds the first line before/after the current cursor position
 * matching one of a list of regular expressions
 * 
 * @param regExps the list of expressions to match
 * @param before  whether to search before the cursor (true) or after (false)
 * 
 * @returns {
 *   line     the line matched
 *   index    the index of the regular expression which matched
 * } on success or undefined on failure
 */
function findLine(regExps, before) {
    var pos = view.cursorPosition();
    pos.column = 0;
    var lastLine = document.documentEnd().line;
    while( pos.isValid() &&  pos.line <= lastLine ) {
        var curLine = document.line(pos.line);
        for(var i=0; i<regExps.length; i++) {
            var match = curLine.match(regExps[i]);
            if (match) {
                return {line: pos.line, index: i};
            }
        }
        if (before) {
            pos.line--;   
        } else {
            pos.line++;
        }
    }
    debug("Error searching fromv " + view.cursorPosition() + " to " + pos + " for " + regExps);
}

function resolve_conflict() {
    var mid;
    var conflict_start = findLine([/^<<<<<<</], true);
    var conflict_end = findLine([new RegExp("^>>>>>>>")], false);;
    var keepTextRange;
    
    mid = findLine([/^=======/, /^<<<<<<</], true);
    if (mid.index == 0) {
        if (mid.line == conflict_end.line-1) {
            keepTextRange = undefined;
        } else {
            keepTextRange = new Range(mid.line+1,0,conflict_end.line, 0)            
        }
    } else {
        mid = findLine([/^=======/], false);
        if (mid.line == conflict_start.line+1) {
            keepTextRange = undefined;
        } else {
            keepTextRange = new Range(conflict_start.line+1,0,mid.line, 0)
        }
    }
    var keep_text;
    if (keepTextRange) {
        keep_text = document.text(keepTextRange);
    }
    document.editBegin()
    document.removeText(conflict_start.line,0, conflict_end.line+1,0)
    if (keepTextRange) {
        document.insertText(conflict_start.line,0, keep_text);        
    }
    document.editEnd()
    next_conflict()
}

function next_conflict() {goto_conflict(true)}
function prev_conflict() {goto_conflict(false)}

function goto_conflict(next) {
    var next_mid = findLine([/^=======/], !next);
    if (next_mid) {
        view.setCursorPosition(next_mid.line, 0);
    }
}
