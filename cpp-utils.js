var katescript = {
    "author": "Jonathan L. Verner <jonathan.verner@matfyz.cz>",
    "license": "LGPLv2+",
    "revision": 1,
    "kate-version": "5.1",
    "functions": ["disable_copy"],
    "actions": [
        {   "function": "disable_copy",
            "name": "Disable copy (delete the copy-constructor & = operator)",
            "icon": "",
            "category": "Editing",
            "interactive": "false"
        }
    ]
}; // kate-script-header, must be at the start of the file without comments

require ("range.js");
require ("cursor.js");

function getContainingClassName(pos) {
    var classRE = /^\s*class\s\s*([a-zA-Z][^ +-.()&:@#$%![\]]*)/;
    pos.column = 0;
    while( pos.isValid() ) {
        var curLineRange = new Range(pos.line, 0, pos.line+1, 0);
        var curLine = document.text(curLineRange);
        var match = curLine.match(classRE);
        if (match)
            return match[1];
        pos.line--;
    }
}

function insert_indented(pos, lines) {
    var indent="";
    var text="";
    for(var i=0;i<pos.column;i++) {
        indent+=" ";
    }
    for(var j=0;j<lines.length;j++) {
        if (lines[j].length > 0) {
            text += indent+lines[j]+"\n";
        } else {
            text += "\n";
        }
    }
    pos.column = 0;
    document.insertText(pos, text);
}

function disable_copy() {
    var pos = view.cursorPosition();
    var cls = getContainingClassName(pos.clone());
    if (cls) {
        insert_indented(pos.clone(), [
            "",
            "// Disable copying",
            cls+"(const "+cls+"&) = delete;",
            cls+"& operator=(const "+cls+"&) = delete;",
            ""
        ]);
    }
}
